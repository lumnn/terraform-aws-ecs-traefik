terraform {
  required_providers {
    aws = {
      version = "~> 4.13"
      source  = "hashicorp/aws"
    }
  }
}

locals {
  project_name_title = replace(title(replace(var.project_name, "_", " "))," ","")
}

data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

data "aws_vpc" "selected" {
  tags = {
    Name = var.vpc_name
  }
}

data "aws_subnets" "selected" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.selected.id]
  }
}
