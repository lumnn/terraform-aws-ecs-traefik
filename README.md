# Traefik on AWS ECS

> A terraform module to deploy Traefik Proxy on AWS ECS. To be used for development purposes

It includes following resources:

- ECS Task definition with traefik
- CloudWatch log group for Task Definition (7 days retention)
- EFS for traefik storage
- IAM Task execution role for Traefik to be able to read EC2 data as well as automatically obtain certificates with letsencrypt
