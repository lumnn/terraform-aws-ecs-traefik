resource "aws_cloudwatch_log_group" "traefik" {
  name = "/ecs/${var.project_name}"
  retention_in_days = 7
}
