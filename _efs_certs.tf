resource "aws_efs_file_system" "storage" {
  tags = {
    Name = "${var.project_name}"
  }
}

resource "aws_efs_mount_target" "efs_mount" {
  for_each = toset(data.aws_subnets.selected.ids)

  file_system_id  = aws_efs_file_system.storage.id
  subnet_id       = each.value
  security_groups = [aws_security_group.storage.id]
}

resource "aws_security_group" "storage" {
  name = "${var.project_name}-efs"
  vpc_id = data.aws_vpc.selected.id

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = var.storage_efs_allow_sg
  }
}
