data "aws_route53_zone" "route53_allowed_domains" {
  count = length(var.route53_allowed_domains)
  name = var.route53_allowed_domains[count.index]
}

resource "aws_iam_role" "traefik_ecs_task_execution_role" {
  name = "${local.project_name_title}ECSTaskExecutionRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name = "${local.project_name_title}ECSEC2Read"
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Sid = "TraefikECSEC2Read"
          Effect = "Allow"
          Action = [
            "ecs:DescribeContainerInstances",
            "ecs:DescribeClusters",
            "ecs:DescribeTasks",
            "ecs:ListTasks",
            "ec2:DescribeInstances",
            "ecs:DescribeTaskDefinition",
            "ecs:ListClusters"
          ],
          Resource = [
            "*"
          ]
        }
      ]
    })
  }

  dynamic "inline_policy" {
    for_each = length(var.route53_allowed_domains) > 0 ? [1] : []

    content {
      name = "${local.project_name_title}LetsEncryptDNSAuth"
      policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
          {
            Sid = "Read"
            Effect = "Allow",
            Action = [
              "route53:ListHostedZonesByName"
            ],
            Resource = [
              "*"
            ]
          },
          {
            Sid = "Write"
            Effect = "Allow"
            Action = [
              "route53:GetChange",
              "route53:ChangeResourceRecordSets",
              "route53:ListResourceRecordSets"
            ],
            Resource = concat(
              [
                for domain in data.aws_route53_zone.route53_allowed_domains:
                "arn:aws:route53:::hostedzone/${domain.zone_id}"
              ],
              [
                for domain in data.aws_route53_zone.route53_allowed_domains:
                "arn:aws:route53:::change/*"
              ]
            )
          }
        ]
      })
    }
  }
}
