resource "aws_ecs_task_definition" "traefik" {
  family                   = var.project_name
  requires_compatibilities = ["EC2"]
  execution_role_arn       = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/ecsTaskExecutionRole"
  task_role_arn            = aws_iam_role.traefik_ecs_task_execution_role.arn

  container_definitions    = jsonencode([
    {
      name               = "traefik"
      image              = var.docker_image
      essential          = true
      memoryReservation  = 256
      command            = var.traefik_args
      portMappings = [
        for port in var.exposed_tcp_ports:
        {
          containerPort = port
          hostPort      = var.exposed_tcp_ports_random ? 0 : port
          protocol      = "tcp"
        }
      ],
      dockerLabels = var.traefik_labels,
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.traefik.name
          awslogs-region        = data.aws_region.current.name
          awslogs-stream-prefix = "ecs"
        }
      }
      mountPoints = [
        {
          sourceVolume  = "storage"
          containerPath = "/var/traefik-storage"
        }
      ]
    }
  ])

  volume {
    name = "storage"

    efs_volume_configuration {
      file_system_id     = aws_efs_file_system.storage.id
      transit_encryption = "ENABLED"
    }
  }
}
