variable "project_name" {
  description = "A underscore separated name of deployed module. Will influence names of tasks, roles, etc."
  type = string
  default = "traefik"
}

variable "docker_image" {
  description = "A docker image to use"
  type = string
  default = "traefik:v2.9"
}

variable "ecs_cluster" {
  description = "Name of the cluster where task will be started"
  type = string
  default = "default"
}

variable "traefik_args" {
  description = "Arguments to pass into Traefik cmd"
  type = list(string)
  default = []
}

variable "traefik_labels" {
  description = "Docker Labels to put on Traefik instance"
  type = map(string)
  default = {}
}

variable "exposed_tcp_ports" {
  description = "Ports to expose"
  type = list(number)
  default = [80, 443]
}

variable "exposed_tcp_ports_random" {
  description = "Whether use 0 as hostPort or keep it same to containerPort. This will assign random ports on server"
  type = bool
  default = true
}

variable "route53_allowed_domains" {
  description = "Domains to which allow enough access for letsencrypt verifications"
  type = list(string)
  default = []
}

variable "vpc_name" {
  description = "Name of the VPC. This influences the mount targets for EFS certificate storage"
  type = string
  default = "default"
}

variable "storage_efs_allow_sg" {
  description = "List of security groups that should be allowed to access EFS network drive (port 2049) with backups. "
  type = list(string)
  nullable = false
}
